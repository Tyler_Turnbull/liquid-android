package com.tsquaredapplications.liquid.common.database.entry

import android.os.Parcelable
import com.tsquaredapplications.liquid.common.database.icons.Icon
import com.tsquaredapplications.liquid.common.database.presets.Preset
import com.tsquaredapplications.liquid.common.database.types.DrinkType
import kotlinx.android.parcel.Parcelize
import kotlin.math.max

@Parcelize
class EntryDataWrapper(
    val entry: Entry,
    val drinkType: DrinkType,
    val preset: Preset?,
    val icon: Icon
) : Parcelable

fun List<EntryDataWrapper>.totalHydratingAmount(): Int =
    filter { it.drinkType.hydration > 0 }.map { it.entry.amount * it.drinkType.hydration }.sum()
        .toInt()

fun List<EntryDataWrapper>.totalDehydratingAmount(): Int =
    filter { it.drinkType.hydration < 0 }.map { it.entry.amount * -(it.drinkType.hydration) }
        .sum().toInt()

fun List<EntryDataWrapper>.getProgress() = map { it.entry.amount * it.drinkType.hydration }.sum()

fun List<EntryDataWrapper>.getProgressPercentage(goal: Int): Int =
    max(((getProgress() / goal) * 100).toInt(), 0)

fun List<EntryDataWrapper>.toBasicProgress(dailyGoal: Int): BasicProgress {
    val progress = map { it.entry.amount * it.drinkType.hydration }.sum()
    val percentComplete = max(((progress / dailyGoal) * 100).toInt(), 0)

    return BasicProgress(
        goalPercentage = percentComplete,
        hydratingAmount = totalHydratingAmount(),
        dehydratingAmount = totalDehydratingAmount()
    )
}

fun List<EntryDataWrapper>.toDetailedProgress(dailyGoal: Int): DetailedProgress {
    val progress = map { it.entry.amount * it.drinkType.hydration }.sum()
    val percentComplete = max(((progress / dailyGoal) * 100).toInt(), 0)

    val mostRecentEntry = lastOrNull()
    val previousPercentComplete = if (mostRecentEntry == null) {
        0
    } else {
        val previousProgress =
            progress - (mostRecentEntry.entry.amount.toInt() * mostRecentEntry.drinkType.hydration)

        max(((previousProgress / dailyGoal) * 100).toInt(), 0)
    }

    val currentHydratingAmount = totalHydratingAmount()
    val previousHydratingAmount: Int = when {
        mostRecentEntry == null -> 0
        mostRecentEntry.drinkType.hydration < 0 -> currentHydratingAmount
        else -> {
            currentHydratingAmount - (mostRecentEntry.entry.amount * mostRecentEntry.drinkType.hydration).toInt()
        }
    }

    val currentDehydratingAmount = totalDehydratingAmount()
    val previousDehydratingAmount: Int = when {
        mostRecentEntry == null -> 0
        mostRecentEntry.drinkType.hydration > 0 -> currentDehydratingAmount
        else -> {
            currentDehydratingAmount - (mostRecentEntry.entry.amount * -(mostRecentEntry.drinkType.hydration)).toInt()
        }
    }

    return DetailedProgress(
        percentComplete,
        previousPercentComplete,
        currentHydratingAmount,
        previousHydratingAmount,
        currentDehydratingAmount,
        previousDehydratingAmount
    )
}

class BasicProgress(
    val goalPercentage: Int,
    val hydratingAmount: Int,
    val dehydratingAmount: Int
)

class DetailedProgress(
    val currentGoalPercentage: Int,
    val previousGoalPercentage: Int,
    val currentHydratingAmount: Int,
    val previousHydratingAmount: Int,
    val currentDehydratingAmount: Int,
    val previousDehydratingAmount: Int
)