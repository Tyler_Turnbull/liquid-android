package com.tsquaredapplications.liquid.common.database.icons

import android.content.Context
import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Icon(
    @PrimaryKey val iconUid: Int,
    val iconResourceName: String,
    val largeIconResourceName: String
) : Parcelable {

    @DrawableRes
    fun getIconResourceId(context: Context): Int =
        context.resources.getIdentifier(iconResourceName, "drawable", context.packageName)

    @DrawableRes
    fun getLargeIconResourceId(context: Context): Int =
        context.resources.getIdentifier(largeIconResourceName, "drawable", context.packageName)

}