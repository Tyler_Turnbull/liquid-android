package com.tsquaredapplications.liquid.common.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.tsquaredapplications.liquid.R
import com.tsquaredapplications.liquid.common.database.entry.Entry
import com.tsquaredapplications.liquid.common.database.entry.EntryDao
import com.tsquaredapplications.liquid.common.database.goal.Goal
import com.tsquaredapplications.liquid.common.database.goal.GoalDao
import com.tsquaredapplications.liquid.common.database.icons.Icon
import com.tsquaredapplications.liquid.common.database.icons.IconDao
import com.tsquaredapplications.liquid.common.database.presets.Preset
import com.tsquaredapplications.liquid.common.database.presets.PresetDao
import com.tsquaredapplications.liquid.common.database.types.DrinkType
import com.tsquaredapplications.liquid.common.database.types.DrinkTypeDao
import java.util.concurrent.Executors

@Database(
    entities = [DrinkType::class, Icon::class, Preset::class, Entry::class, Goal::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun drinkTypeDao(): DrinkTypeDao
    abstract fun iconDao(): IconDao
    abstract fun presetDao(): PresetDao
    abstract fun entryDao(): EntryDao
    abstract fun goalDao(): GoalDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context,
            AppDatabase::class.java, "LiquidDatabase.db"
        )
            .addCallback(object : Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    Executors.newSingleThreadExecutor().execute {
                        getInstance(context).drinkTypeDao()
                            .insertAll(prePopulateDrinkTypes(context))
                        getInstance(context).iconDao().insertAll(prePopulateIconTypes(context))
                    }
                }
            })
            .build()

        fun prePopulateDrinkTypes(context: Context) =
            listOf(
                DrinkType(1, context.getString(R.string.beer), -1.0, true, 1),
                DrinkType(2, context.getString(R.string.cocktail), -1.0, true, 2),
                DrinkType(3, context.getString(R.string.coffee), 1.0, false, 3),
                DrinkType(4, context.getString(R.string.energy_drink), 0.5, false, 4),
                DrinkType(5, context.getString(R.string.iced_tea), 1.0, false, 5),
                DrinkType(6, context.getString(R.string.juice), 1.0, false, 6),
                DrinkType(7, context.getString(R.string.milk), 1.0, false, 7),
                DrinkType(8, context.getString(R.string.smoothie), 1.0, false, 8),
                DrinkType(9, context.getString(R.string.soda), 1.0, false, 9),
                DrinkType(10, context.getString(R.string.sparkling_water), 1.0, false, 10),
                DrinkType(11, context.getString(R.string.spirit), -8.0, true, 11),
                DrinkType(12, context.getString(R.string.sports_drink), 1.0, false, 12),
                DrinkType(13, context.getString(R.string.tea), 1.0, false, 13),
                DrinkType(14, context.getString(R.string.water), 1.0, false, 14),
                DrinkType(15, context.getString(R.string.wine), -2.66, true, 15)
            )

        fun prePopulateIconTypes(context: Context) =
            listOf(
                Icon(
                    1,
                    context.resources.getResourceEntryName(R.drawable.beer),
                    context.resources.getResourceName(R.drawable.beer_large)
                ),
                Icon(
                    2,
                    context.resources.getResourceName(R.drawable.cocktail),
                    context.resources.getResourceName(R.drawable.cocktail_large)
                ),
                Icon(
                    3,
                    context.resources.getResourceName(R.drawable.coffee),
                    context.resources.getResourceName(R.drawable.coffee_large)
                ),
                Icon(
                    4,
                    context.resources.getResourceName(R.drawable.energy_drink),
                    context.resources.getResourceName(R.drawable.energy_drink_large)
                ),
                Icon(
                    5,
                    context.resources.getResourceName(R.drawable.iced_tea),
                    context.resources.getResourceName(R.drawable.iced_tea_large)
                ),
                Icon(
                    6,
                    context.resources.getResourceName(R.drawable.juice),
                    context.resources.getResourceName(R.drawable.juice_large)
                ),
                Icon(
                    7,
                    context.resources.getResourceName(R.drawable.milk),
                    context.resources.getResourceName(R.drawable.milk_large)
                ),
                Icon(
                    8,
                    context.resources.getResourceName(R.drawable.smoothie),
                    context.resources.getResourceName(R.drawable.smoothie_large)
                ),
                Icon(
                    9,
                    context.resources.getResourceName(R.drawable.soda),
                    context.resources.getResourceName(R.drawable.soda_large)
                ),
                Icon(
                    10,
                    context.resources.getResourceName(R.drawable.sparkling_water),
                    context.resources.getResourceName(R.drawable.sparkling_water_large)
                ),
                Icon(
                    11,
                    context.resources.getResourceName(R.drawable.spirit),
                    context.resources.getResourceName(R.drawable.spirit_large)
                ),
                Icon(
                    12,
                    context.resources.getResourceName(R.drawable.sports_drink),
                    context.resources.getResourceName(R.drawable.sports_drink_large)
                ),
                Icon(
                    13,
                    context.resources.getResourceName(R.drawable.tea),
                    context.resources.getResourceName(R.drawable.tea_large)
                ),
                Icon(
                    14,
                    context.resources.getResourceName(R.drawable.water),
                    context.resources.getResourceName(R.drawable.water_large)
                ),
                Icon(
                    15,
                    context.resources.getResourceName(R.drawable.wine),
                    context.resources.getResourceName(R.drawable.wine_large)
                ),
                Icon(
                    16,
                    context.resources.getResourceName(R.drawable.bottle),
                    context.resources.getResourceName(R.drawable.bottle_large)
                ),
                Icon(
                    17,
                    context.resources.getResourceName(R.drawable.bourbon),
                    context.resources.getResourceName(R.drawable.bourbon_large)
                ),
                Icon(
                    18,
                    context.resources.getResourceName(R.drawable.pink_tea_cup),
                    context.resources.getResourceName(R.drawable.pink_tea_cup_large)
                ),
                Icon(
                    19,
                    context.resources.getResourceName(R.drawable.pink_tini),
                    context.resources.getResourceName(R.drawable.pink_tini_large)
                ),
                Icon(
                    20,
                    context.resources.getResourceName(R.drawable.small_bottle),
                    context.resources.getResourceName(R.drawable.small_bottle_large)
                ),
                Icon(
                    21,
                    context.resources.getResourceName(R.drawable.travel_mug),
                    context.resources.getResourceName(R.drawable.travel_mug_large)
                )
            )
    }
}