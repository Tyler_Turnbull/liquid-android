package com.tsquaredapplications.liquid.common.database.icons

import android.content.Context
import com.tsquaredapplications.liquid.R

interface IconRepository {
    suspend fun getAllIcons(): Map<Int, Icon>

    companion object {
        fun getDefaultIcon(context: Context) = Icon(
            -1,
            context.resources.getResourceName(R.drawable.drink_placeholder),
            context.resources.getResourceName(R.drawable.drink_type_placeholder_large)
        )
    }
}