package com.tsquaredapplications.liquid.common.database.types

import android.content.Context
import com.tsquaredapplications.liquid.common.database.icons.IconRepository
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class RoomTypeRepository
@Inject constructor(
    @ApplicationContext private val context: Context,
    private val drinkTypeDao: DrinkTypeDao,
    private val iconRepository: IconRepository
) : TypeRepository {
    override suspend fun getAllDrinkTypesWithIcons(): Map<Int, DrinkTypeAndIcon> {
        val drinkTypes = drinkTypeDao.getAll()
        val icons = iconRepository.getAllIcons()

        return drinkTypes.map { drinkType ->
            drinkType.drinkTypeUid to DrinkTypeAndIcon(
                drinkType,
                icons[drinkType.iconUid] ?: IconRepository.getDefaultIcon(context)
            )
        }.toMap()
    }

    override suspend fun getAllDrinkTypes(): Map<Int, DrinkType> =
        drinkTypeDao.getAll().map { it.drinkTypeUid to it }.toMap()
}
