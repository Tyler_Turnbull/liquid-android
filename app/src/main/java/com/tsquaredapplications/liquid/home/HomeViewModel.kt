package com.tsquaredapplications.liquid.home

import androidx.lifecycle.viewModelScope
import com.tsquaredapplications.liquid.common.BaseViewModel
import com.tsquaredapplications.liquid.common.database.entry.EntryRepository
import com.tsquaredapplications.liquid.common.database.entry.toBasicProgress
import com.tsquaredapplications.liquid.common.database.entry.toDetailedProgress
import com.tsquaredapplications.liquid.common.database.users.UserInformation
import com.tsquaredapplications.liquid.ext.getStartAndEndTimeForToday
import com.tsquaredapplications.liquid.home.HomeViewModel.HomeState.Initialize
import com.tsquaredapplications.liquid.home.resources.HomeResourceWrapper
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel
@Inject constructor(
    private val userInformation: UserInformation,
    private val resourceWrapper: HomeResourceWrapper,
    private val entryRepository: EntryRepository
) : BaseViewModel<HomeViewModel.HomeState>() {

    fun start(withAnimation: Boolean) {
        state.value = Initialize(
            hydratingText = resourceWrapper.hydratingText,
            dehydratingText = resourceWrapper.dehydratingText
        )

        updateProgress(withAnimation)
    }

    private fun updateProgress(withAnimation: Boolean) {
        viewModelScope.launch {
            val (startTime, endTime) = getStartAndEndTimeForToday()
            val todayEntries = entryRepository.getAllInTimeRange(from = startTime, to = endTime)

            if (!withAnimation) {
                with(todayEntries.toBasicProgress(userInformation.dailyGoal)) {
                    state.value = HomeState.SetProgress(
                        goalPercentage = goalPercentage,
                        hydratingAmount = hydratingAmount,
                        dehydratingAmount = dehydratingAmount
                    )
                }
            } else {
                with(todayEntries.toDetailedProgress(userInformation.dailyGoal)) {
                    state.value = HomeState.UpdateProgress(
                        currentGoalPercentage,
                        previousGoalPercentage,
                        currentHydratingAmount,
                        previousHydratingAmount,
                        currentDehydratingAmount,
                        previousDehydratingAmount
                    )
                }
            }
        }
    }

    sealed class HomeState {
        class Initialize(
            val hydratingText: String,
            val dehydratingText: String
        ) : HomeState()

        class SetProgress(
            val goalPercentage: Int,
            val hydratingAmount: Int,
            val dehydratingAmount: Int
        ) : HomeState()

        class UpdateProgress(
            val currentGoalPercentage: Int,
            val previousGoalPercentage: Int,
            val currentHydratingAmount: Int,
            val previousHydratingAmount: Int,
            val currentDehydratingAmount: Int,
            val previousDehydratingAmount: Int
        ) : HomeState()
    }
}