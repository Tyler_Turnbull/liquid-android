package com.tsquaredapplications.liquid.test.util.mocks

import com.tsquaredapplications.liquid.common.database.icons.Icon
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals

fun mockWaterIcon(): Icon = mockk {
    every { iconUid } returns MOCK_WATER_ICON_UID
    every { iconResourceName } returns MOCK_WATER_ICON_RESOURCE_NAME
    every { largeIconResourceName } returns MOCK_WATER_LARGE_ICON_RESOURCE_NAME
}

fun Icon.assertWaterIcon() {
    assertEquals(MOCK_WATER_ICON_UID, iconUid)
    assertEquals(MOCK_WATER_ICON_RESOURCE_NAME, iconResourceName)
    assertEquals(MOCK_WATER_LARGE_ICON_RESOURCE_NAME, largeIconResourceName)
}

fun mockWaterPresetIcon(): Icon = mockk {
    every { iconUid } returns MOCK_WATER_PRESET_ICON_UID
    every { iconResourceName } returns MOCK_WATER_PRESET_ICON_RESOURCE_NAME
    every { largeIconResourceName } returns MOCK_WATER_PRESET_LARGE_ICON_RESOURCE_NAME
}

fun Icon.assertWaterPresetIcon() {
    assertEquals(MOCK_WATER_PRESET_ICON_UID, iconUid)
    assertEquals(MOCK_WATER_PRESET_ICON_RESOURCE_NAME, iconResourceName)
    assertEquals(MOCK_WATER_PRESET_LARGE_ICON_RESOURCE_NAME, largeIconResourceName)
}

fun mockBeerIcon(): Icon = mockk {
    every { iconUid } returns MOCK_BEER_ICON_UID
    every { iconResourceName } returns MOCK_BEER_ICON_RESOURCE_NAME
    every { largeIconResourceName } returns MOCK_BEER_LARGE_ICON_RESOURCE_NAME
}

fun Icon.assertBeerIcon() {
    assertEquals(MOCK_BEER_ICON_UID, iconUid)
    assertEquals(MOCK_BEER_ICON_RESOURCE_NAME, iconResourceName)
    assertEquals(MOCK_BEER_LARGE_ICON_RESOURCE_NAME, largeIconResourceName)
}

fun mockBeerPresetIcon(): Icon = mockk {
    every { iconUid } returns MOCK_BEER_PRESET_ICON_UID
    every { iconResourceName } returns MOCK_BEER_PRESET_ICON_RESOURCE_NAME
    every { largeIconResourceName } returns MOCK_BEER_PRESET_LARGE_ICON_RESOURCE_NAME
}

fun Icon.assertBeerPresetIcon() {
    assertEquals(MOCK_BEER_PRESET_ICON_UID, iconUid)
    assertEquals(MOCK_BEER_PRESET_ICON_RESOURCE_NAME, iconResourceName)
    assertEquals(MOCK_BEER_PRESET_LARGE_ICON_RESOURCE_NAME, largeIconResourceName)
}

fun mockTeaIcon(): Icon = mockk {
    every { iconUid } returns MOCK_TEA_ICON_UID
    every { iconResourceName } returns MOCK_TEA_ICON_RESOURCE_NAME
    every { largeIconResourceName } returns MOCK_TEA_LARGE_ICON_RESOURCE_NAME
}

fun Icon.assertTeaIcon() {
    assertEquals(MOCK_TEA_ICON_UID, iconUid)
    assertEquals(MOCK_TEA_ICON_RESOURCE_NAME, iconResourceName)
    assertEquals(MOCK_TEA_LARGE_ICON_RESOURCE_NAME, largeIconResourceName)
}

fun mockTeaPresetIcon(): Icon = mockk {
    every { iconUid } returns MOCK_TEA_PRESET_ICON_UID
    every { iconResourceName } returns MOCK_TEA_PRESET_ICON_RESOURCE_NAME
    every { largeIconResourceName } returns MOCK_TEA_PRESET_LARGE_ICON_RESOURCE_NAME
}

fun Icon.assertTeaPresetIcon() {
    assertEquals(MOCK_TEA_PRESET_ICON_UID, iconUid)
    assertEquals(MOCK_TEA_PRESET_ICON_RESOURCE_NAME, iconResourceName)
    assertEquals(MOCK_TEA_PRESET_LARGE_ICON_RESOURCE_NAME, largeIconResourceName)
}

const val MOCK_WATER_ICON_UID = 1
const val MOCK_WATER_ICON_RESOURCE_NAME = "MOCK_WATER_ICON_RESOURCE_NAME"
const val MOCK_WATER_LARGE_ICON_RESOURCE_NAME = "MOCK_WATER_LARGE_ICON_RESOURCE_NAME"

const val MOCK_BEER_ICON_UID = 2
const val MOCK_BEER_ICON_RESOURCE_NAME = "MOCK_BEER_ICON_RESOURCE_NAME"
const val MOCK_BEER_LARGE_ICON_RESOURCE_NAME = "MOCK_BEER_LARGE_ICON_RESOURCE_NAME"

const val MOCK_TEA_ICON_UID = 3
const val MOCK_TEA_ICON_RESOURCE_NAME = "MOCK_TEA_ICON_RESOURCE_NAME"
const val MOCK_TEA_LARGE_ICON_RESOURCE_NAME = "MOCK_TEA_LARGE_ICON_RESOURCE_NAME"

const val MOCK_WATER_PRESET_ICON_UID = 4
const val MOCK_WATER_PRESET_ICON_RESOURCE_NAME = "MOCK_WATER_PRESET_ICON_RESOURCE_NAME"
const val MOCK_WATER_PRESET_LARGE_ICON_RESOURCE_NAME = "MOCK_WATER_PRESET_LARGE_ICON_RESOURCE_NAME"

const val MOCK_BEER_PRESET_ICON_UID = 5
const val MOCK_BEER_PRESET_ICON_RESOURCE_NAME = "MOCK_BEER_PRESET_ICON_RESOURCE_NAME"
const val MOCK_BEER_PRESET_LARGE_ICON_RESOURCE_NAME = "MOCK_BEER_PRESET_LARGE_ICON_RESOURCE_NAME"

const val MOCK_TEA_PRESET_ICON_UID = 6
const val MOCK_TEA_PRESET_ICON_RESOURCE_NAME = "MOCK_TEA_PRESET_ICON_RESOURCE_NAME"
const val MOCK_TEA_PRESET_LARGE_ICON_RESOURCE_NAME = "MOCK_TEA_PRESET_LARGE_ICON_RESOURCE_NAME"